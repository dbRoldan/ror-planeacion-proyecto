---
layout: post
title:  "Informe Clase 1"
date:   2018-08-01 12:00:00 -0500
reference: clase
categories: clase informe
frase: "Usted Gana Lo Que Quiere"
description: "Primera Clase, se ponen las reglas y se hacen un analisis de la actual ingenieria..."
---
## Mapa Mental
<a href="{{ site.baseurl }}/MapasMentales/Clase1.png">![Imagen Mapa Mental]({{ site.baseurl }}/MapasMentales/Clase1.png "Mapa Mental 1")</a>

## Datos de la Clase
### Objetivo
- Entender la forma de construir todo el conocimiento y se dan las normas del curso comenzando en la forma de generar conceptos y características se nos da el objetivo de ser buenas personas, ingenieros y en general en nuestras vidas ser buenos en lo que hagamos, hallar el cometido enfocado en entender la metodología, el entender que las clases serán para gerentes y el conocimiento se da de esa manera.

### Lo que Aprendi
Entendiendo que la primera clase se centra en el hecho de la construcción se basa en el diseño de la implementación que se caracterizo y basándose en un concepto construido diseñado en una construcción y el desarrollo de un ingeniero basado en la teoría general de sistemas que tiene una educación centrada y vista como una obligación para la construcción y la implementación de un desarrollo centrado en toma de decisiones basada en el uso de la información como un eje primario para la toma de decisiones como administración y gerencia centrada en la planeación estratégica basada en un mundo complejo y sistémico.

### Tabla de Datos

| Consultas                                                | Biografias     | Libros         | Videos                 |
| ---------                                                | ----------     | ------         | ------                 |
| Que es la Vida                                           | Zygmunt Bauman | Mapas Mentales | Copiando la Naturaleza |
| Qué es lo que se necesita para ser ingeniero de sistemas | Eduardo Punset |                | Llamada a la Crítica   |
| Como se comporta la ingeniería de sistemas en el 2025    |                |                |                        |
| Que es la ingeniería de Sistemas                         |                |                |                        |
| Cuales Son las Herramientas                              |                |                |                        |


## Links de Documentos
- [Imagen de Mapa Mental]({{ site.baseurl }}/MapasMentales/Clase1.png "Imagen de Mapa Mental")
- [Informe de Clase]({{ site.baseurl }}/NotasClase/Clase1.pdf "Informe Clase"){:target="_blank"}
- [Documento respectiva a Mapa Mental]({{ site.baseurl }}/MapasMentales/Clase1.pdf "Presentacion Respectiva a Clase"){:target="_blank"}
- [Mapa Mental]({{ site.baseurl }}/MapasMentales/Clase1.xmind "Mapa Mental")
