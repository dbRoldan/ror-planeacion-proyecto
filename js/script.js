//Canvas container
var p5div = document
  .querySelector('#p5sketch');

//Canvas size
var w, h;

//P5.js sketch
var p5sketch = ( p5 ) => {
  
  p5.setup = () => {
    w = p5div.clientWidth;
    h = p5div.clientHeight;
    p5.createCanvas(w,h);
  };
  
  p5.windowResized = () => {
    w = p5div.clientWidth;
    h = p5div.clientHeight;
    
    p5.resizeCanvas(w,h);
  }
  
  p5.polygon = (
    x, y, w, h, np, rot=0) =>
  {
    var step = p5.TWO_PI/np;
    var a = w/2, b = h/2, r = 0;
    var pnts = [];
    
    for(
      let th=rot; 
      th<rot+p5.TWO_PI; 
      th+=step) 
    {
      if(p5.abs(p5.tan(th)) <= b/a)
        r = a/p5.abs(p5.cos(th));
      else
        r = b/p5.abs(p5.sin(th));
      pnts.push({
        x: x + r*p5.cos(th),
        y: y + r*p5.sin(th) 
      });
    }
    
    p5.beginShape();
      for(let i=0; i<pnts.length; i++){
        p5.vertex(
          pnts[i].x,
          pnts[i].y,
        );
        p5.vertex(
          pnts[(i+2)%pnts.length].x,
          pnts[(i+2)%pnts.length].y,
        );
        p5.vertex(
          pnts[(i+4)%pnts.length].x,
          pnts[(i+4)%pnts.length].y,
        );
        p5.vertex(
          pnts[(i+6)%pnts.length].x,
          pnts[(i+6)%pnts.length].y,
        );
      }
    p5.endShape(p5.CLOSE);
  }
  
  var t = 0, t2 = 0;
  var rnd1 = p5.random(30, 100);
  var rnd2 = p5.random(0, 0.5);
  p5.draw = () => {
    p5.clear();
    p5.noFill();
    p5.stroke("#101010");
    
    if(p5.frameCount%200 == 0){
      rnd1 = p5.random(30, 100);
      rnd2 = p5.random(0, 0.5);
    }
    
    for(let i=4; i<=30; i++){
      p5.strokeWeight(
        p5.sin(t2)*0.1 + 0.2);
      p5.polygon(
       w/2, h/2, 
       w-i*rnd2*(w/rnd1)*p5.sin(t2*i), 
       h-i*rnd2*(h/rnd1)*p5.sin(t2*i), 
       i, t);
    }
    t += 0.001
    t2 += 0.01
  }

}

//P5.js sketch instance
var p5inst = new p5(p5sketch, p5div);